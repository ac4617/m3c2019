!Final project part 2
!This file contains 1 module, 1 main program and 4 subroutines:
! params: module contain problem parameters and useful constants
! crickets: main program which reads in parameters from data.in
! 	calls simulate, and writes the computed results to output files
! simulate: subroutine for simulating coupled oscillator model
! 	using explicit-Euler time-marching and distributed-memory
! 	parallelization
! RHS: subroutine called by simulate, generates right-hand side
!		of oscillator model equations
! MPE_DECOMP1D: subroutine called by simulate and used to assign
!		oscillators to processes
! random_normal: subroutine called by main program and used to generate
!		natural frequencies, w

!-------------------------------------------------------------
module params
	use mpi
	implicit none
	real(kind=8), parameter :: pi = acos(-1.d0)
	complex(kind=8), parameter :: ii=cmplx(0.0,1.0) !ii = sqrt(-1)
    integer :: ntotal !total number of oscillators,
	real(kind=8) :: c,mu,sigma !coupling coefficient, mean, std for computing omega
	integer :: nlocal_min
	save
end module params
!-------------------------------

program crickets
    use mpi
    use params
    implicit none
    integer :: i1,j1
    integer :: nt !number of time steps
    real(kind=8) :: dt!time step
    integer :: myid, numprocs, ierr, istart, iend
    real(kind=8), allocatable, dimension(:) :: f0,w,f ! initial phases, frequencies, final phases
    real(kind=8), allocatable, dimension(:) :: r !synchronization parameter

 ! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

!gather input
    open(unit=10,file='data.in')
        read(10,*) ntotal !total number of oscillators
        read(10,*) nt !number of time steps
        read(10,*) dt !size of time step
        read(10,*) c ! coupling parameter
        read(10,*) sigma !standard deviation for omega calculation
    close(10)

    allocate(f0(ntotal),f(ntotal),w(ntotal),r(nt))


!generate initial phases
    call random_number(f0)
    f0 = f0*2.d0*pi


!generate frequencies
    mu = 1.d0
    call random_normal(ntotal,w)
    w = sigma*w+mu

!compute min(nlocal)
		nlocal_min = ntotal
		do i1 = 0,numprocs-1
			call mpe_decomp1d(ntotal,numprocs,i1,istart,iend)
			nlocal_min = min(iend-istart+1,nlocal_min)
		end do


!compute solution
    call simulate(MPI_COMM_WORLD,numprocs,ntotal,0.d0,f0,w,dt,nt,f,r)

!output solution (after collecting solution onto process 0 in simulate)
     call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
     if (myid==0) then
        open(unit=11,file='theta.dat')
        do i1=1,ntotal
            write(11,*) f(i1)
        end do
        close(11)

        open(unit=12,file='R.dat')
        do i1=1,nt
	    		write(12,*) r(i1)
				end do
				close(12)
    	end if
    !can be loaded in python, e.g. theta=np.loadtxt('theta.dat')

    call MPI_FINALIZE(ierr)
end program crickets



subroutine simulate(comm,numprocs,n,t0,y0,w,dt,nt,y,r)
    !explicit Euler method, parallelized with mpi
    !input:
    !comm: MPI communicator
    !numprocs: total number of processes
    !n: number of oscillators
    !t0: initial time
    !y0: initial phases of oscillators
    !w: array of frequencies, omega_i
    !dt: time step
    !nt: number of time steps
    !output: y, final solution
    !r: synchronization parameter at each time step
    use mpi
    use params
    implicit none
    integer, intent (in) :: n,nt
    real(kind=8), dimension(n), intent(in) :: y0,w
    real(kind=8), intent(in) :: t0,dt
    real(kind=8), dimension(n), intent(out) :: y
		real(kind=8), dimension(nt), intent(out) :: r
		real(kind=8), allocatable, dimension(:) :: ylocal,Rpart,wlocal
    real(kind=8) :: t
    integer :: i1,k,istart,iend,nn
    integer :: comm,myid,ierr,numprocs
		integer, allocatable, dimension(:) :: seed,ai
		real(kind=8), allocatable, dimension(:) ::  temp
		integer :: nseed,time
		!add other variables as needed
		real(kind=8), dimension(nlocal_min) ::h1,h2
		integer, dimension(MPI_STATUS_SIZE) :: status
		integer :: receiver1,receiver2, sender1,sender2,k1,k2,j1,j2,request
		real(kind=8),allocatable, dimension(:) :: g1,request1,status1
		real(kind=8), dimension(n) :: b
		complex(kind=8) :: rlocal,rtemp

    call MPI_COMM_RANK(comm, myid, ierr)
    print *, 'start simulate, myid=',myid

    !set initial conditions

		!y = y0
		y=(/1.d0,2.d0,3.d0,4.d0,5.d0,6.d0,7.d0,8.d0,9.d0,10.d0,11.d0,12.d0,13.d0/)
    t = t0
		r=0.d0
    !generate decomposition and allocate sub-domain variables
    call mpe_decomp1d(size(y),numprocs,myid,istart,iend)
    print *, 'istart,iend,threadID=',istart,iend,myid

		nn=iend-istart+1
		!---- allocate size of ylocal for myid

		allocate( ylocal(nn), Rpart(nn),wlocal(2*nn))
		allocate(g1(nn+2*nlocal_min))

		!---- define variables ylocal and wlocal

		ylocal=y(istart:istart + nn -1)
		wlocal(1:nn)=w(istart:istart + nn -1)

		!---- define disps
		do i1 = 0,numprocs-1
			call mpe_decomp1d(ntotal,numprocs,i1,istart,iend)
			nlocal_min = min(iend-istart+1,nlocal_min)
		end do

		!Set coupling ranges, ai
		allocate(ai(iend-istart+1),temp(iend-istart+1))
		call random_seed(size=nseed)
		call system_clock(time)
		allocate(seed(nseed))
		seed= myid
		!seed = myid+time !remove the "+time" to generate same ai each run
		call random_seed(put=seed)
		call random_number(temp)
		ai = 1 + FLOOR((nlocal_min-1)*temp)

		wlocal(nn+1:)=ai
		!print*,'wlocal',wlocal
		!add code as needed

    !time marching
    do k = 1,1

			!-- getting values from process to the right
			h1=0.d0
			h2=0.d0
			g1=0
			g1(nlocal_min+1:nn+nlocal_min)=ylocal

			!-- getting values from process to the right
						if (myid<numprocs-1) then
										receiver1 = myid+1
								else
										 receiver1 = 0
								end if

								if (myid>0) then
										sender1 = myid-1
								else
										sender1 = numprocs-1
						 end if

								call MPI_ISEND(ylocal(nn-nlocal_min+1:nn),nlocal_min,MPI_DOUBLE_PRECISION,receiver1,0,MPI_COMM_WORLD,request,ierr)
								call MPI_RECV(h1,nlocal_min,MPI_DOUBLE_PRECISION,sender1,0,MPI_COMM_WORLD,status,ierr)
								!print*, h1
								g1(1:nlocal_min)=h1
								!g1(nlocal_min+nn+1:nn+2*nlocal_min)=h1
								!print*, 'h1', h1, 'of process', myid
								!print*, 'g1', g1, 'of process', myid

								call MPI_BARRIER(MPI_COMM_WORLD,ierr)

								!getting values from the left
						if (myid>0) then
											receiver2 = myid-1
									else
											receiver2 = numprocs-1
									end if

									if (myid<numprocs-1) then

											sender2 = myid+1
									else
											sender2 = 0
									end if

									!print*, 'sender1', sender1,'receiver1', receiver1
									call MPI_ISEND(ylocal(1:nlocal_min),nlocal_min,MPI_DOUBLE_PRECISION,receiver2,1,MPI_COMM_WORLD,request,ierr)
									call MPI_RECV(h2,nlocal_min,MPI_DOUBLE_PRECISION,sender2,1,MPI_COMM_WORLD,status,ierr)
									g1(nlocal_min+nn+1:nn+2*nlocal_min)=h2
									!g1(1:nlocal_min)=h2
									!print*, 'h2', h2, 'of process', myid
									!print*, 'g1', g1, 'of process', myid

									call MPI_BARRIER(MPI_COMM_WORLD,ierr)

									!print*,'g1',g1

			        call RHS(nn,t,wlocal,g1,Rpart)
							!ylocal=0.d0
			        ylocal= ylocal + dt*Rpart !ylocal must be declared and defined, Rpart must be declared, and
			        !ylocal= Rpart                !should be returned by RHS
							rlocal=0.d0
							rtemp=0.d0
							rlocal=sum(exp(ii*Rpart))
							print*, 'rlocal' , rlocal

							!call MPI_REDUCE(rlocal,rtemp, 1, MPI_Double_complex, MPI_SUM, 0, comm, ierr)

							!call MPI_BARRIER(MPI_COMM_WORLD,ierr)
							if (myid==0) then
							print*, 'rtemp',rtemp
							r(k)= 1/ntotal * abs(rtemp)
							print*, 'r(k)', r(k)
							end if

				!print*, 'ylocal', ylocal
				!compute r, and store on myid==0

    end do

		b=0.d0
		!call MPI_GATHER(ylocal,nn,MPI_DOUBLE_PRECISION,b,nn,MPI_DOUBLE_PRECISION, 0,MPI_COMM_WORLD,ierr)
		if (myid==0) then
			print*,'b' , b
		end if


    print *, 'before collection',myid, maxval(abs(ylocal))


    !collect ylocal from each processor into y on myid=0


    if (myid==0) print *, 'finished',maxval(abs(y))


end subroutine simulate
!-------------------------
subroutine RHS(nn,t,w,f,Rpart)
    !called by simulate
    !Rpart = (1/dt)*(f(t+dt)-f(t))
    use params
		use mpi
    implicit none
    integer, intent(in) :: nn
    real(kind=8), intent(in) :: t
!dimensions of variables below must be added
    real(kind=8), dimension(2*nn), intent(in) :: w
    real(kind=8), dimension(nn+2*nlocal_min), intent(in) :: f
    real(kind=8), dimension(nn), intent(out) :: Rpart
		integer :: receiver1,receiver2, sender1,sender2,ierr,numprocs,k1,k2,j1,j2,request
		real(kind=8), dimension(nn+2*nlocal_min) :: g1,request1,status1
		 integer, dimension(MPI_STATUS_SIZE) :: status

!Add code to compute rhs
	!print*, 'flocal',f
		Rpart=0.d0

		!print*,'nn',nn
		!print*,'f',f
		!print*,'w',w

		do k1=1,nn
			do k2=1,int(w(k1+nn))
			Rpart(k1)= Rpart(k1)+ sin(f(nlocal_min+k1)-f(nlocal_min+k1+k2))
		end do
			do j2=1,int(w(k1+nn))
			Rpart(k1)= Rpart(k1)+ sin(f(nlocal_min+k1)-f(nlocal_min+k1-j2))
		end do
		rpart=w(1:nn)-c/ntotal* rpart
	end do







	!if (myid>0) then
	!		sender1 = myid-1
	!else
	!		sender1 = numprocs-1
	!end if

	!print*, 'sender1', sender1,'receiver1', receiver1
	!call MPI_ISEND(f,nn,MPI_DOUBLE_PRECISION,receiver1,0,MPI_COMM_WORLD,request1,ierr)
	!call MPI_IRECV(h1,nn,MPI_DOUBLE_PRECISION,sender1,MPI_ANY_TAG,MPI_COMM_WORLD,status1,ierr)


	!g1=status(MPI_tag)
	!print*, 'j1',j1
	!print*,'f' ,f
	!Rpart=Rpart+200*h1
	!print*, Rpart
	!print*, 'h1', h1

	!Rpart(myid+1)=Rpart(myid+1)+k1

!---- Receive from the right

	!receiver2=myid

	!if (myid<numprocs-1) then

	!		sender2 = myid+1
	!else
	!		sender2 = 0
	!end if
	!print*, 'sender2', sender2,'receiver2', receiver2
	!call MPI_ISEND(f(myid+2),1,MPI_DOUBLE_PRECISION,receiver2,1,MPI_COMM_WORLD,request,ierr)
	!call MPI_IRECV(k2,1,MPI_DOUBLE_PRECISION,sender2,1,MPI_COMM_WORLD,status,ierr)

	!j1=status(1)

	!print*, 'k2',k2
	!print*, 'j1',j1
	!Rpart(myid+2)=Rpart(myid+2)+k2
	!print*,'f' ,f
	!Rpart=Rpart+10*h2
	!print*, Rpart
	!print*, 'Rpart', Rpart

end subroutine RHS


!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D

!--------------------------------------------------------------------

subroutine random_normal(n,rn)

! Adapted from the following Fortran 77 code
!      ALGORITHM 712, COLLECTED ALGORITHMS FROM ACM.
!      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
!      VOL. 18, NO. 4, DECEMBER, 1992, PP. 434-435.

!  The function random_normal() returns a normally distributed pseudo-random
!  number with zero mean and unit variance.

!  The algorithm uses the ratio of uniforms method of A.J. Kinderman
!  and J.F. Monahan augmented with quadratic bounding curves.

IMPLICIT NONE
integer, intent(in) :: n
real(kind=8), intent(out) :: rn(n)
!     Local variables
integer :: i1
REAL(kind=8)     :: s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472,           &
            r1 = 0.27597, r2 = 0.27846, u, v, x, y, q

!     Generate P = (u,v) uniform in rectangle enclosing acceptance region
do i1=1,n

DO
  CALL RANDOM_NUMBER(u)
  CALL RANDOM_NUMBER(v)
  v = 1.7156d0 * (v - 0.5d0)

!     Evaluate the quadratic form
  x = u - s
  y = ABS(v) - t
  q = x**2 + y*(a*y - b*x)

!     Accept P if inside inner ellipse
  IF (q < r1) EXIT
!     Reject P if outside outer ellipse
  IF (q > r2) CYCLE
!     Reject P if outside acceptance region
  IF (v**2 < -4.d0*LOG(u)*u**2) EXIT
END DO

!     Return ratio of P's coordinates as the normal deviate
rn(i1) = v/u
end do
RETURN


END subroutine random_normal
