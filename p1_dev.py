#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 26 23:16:32 2019

@author: alex
"""

"""MATH96012 2019 Project 1
Alexandre Cahill ,  CID: 01360485
"""
import numpy as np
import matplotlib.pyplot as plt
from math import e
from math import pi
#--------------------------------

def simulate1(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100):
    """Part1: Simulate bacterial colony dynamics
    Input:
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    dt: time step
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach to problem here:
        
    I tackled this problem by vectorizing all our variables so that X and Y are both(Nt+1,N) vectors
    where [X]_i,j represents the x-position of particle j at time i (same for y-position)
    The first row is given by the initial conditions and the rest follows by induction. 
    For the induction I had to use the only for loop of this function. 
    The inductive hypothesis involves a condition on the distances between matrices.
    I therefore created the distance matrix which then enabled me to do the sum of the angles of the particles that satisfy our properties.
    I then updated the speed vectors and the position vectorss and adjusted then modulo L in both directions.  
    """   
    
    
#--------------------------------    
    phi_init = np.random.rand(N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(N))
    Xinit= r_init*np.cos(phi_init)
    Yinit= r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2

    theta_init = np.random.rand(N)*(2*np.pi) #initial directions of motion
    #---------------------

#initialize variables
    
    X = np.zeros((Nt+1,N))
    Y = np.zeros((Nt+1,N))
    theta = np.zeros((Nt+1,N))
    U = np.zeros((Nt+1,N))
    V = np.zeros((Nt+1,N))
    alpha = np.zeros((Nt+1))

    X[0,:] = Xinit
    Y[0,:] = Yinit
    theta[0,:] = theta_init
    distance_matrix = np.zeros((N,N))
    alpha[0] = 1/N * np.abs(np.sum(e**(1j*theta[0,:])))

    for t in range(Nt):
    

    
        # [distance_matrix]_{i,j} represents the distance between the bacteria i and j at time t.
    
        distance_matrix = np.sqrt((np.outer(np.transpose(X[t,:]),np.transpose(np.ones((N))))-np.outer(np.ones((N)),X[t,:]))**2+(np.outer(np.transpose(Y[t,:]),np.transpose(np.ones((N))))-np.outer(np.ones((N)),Y[t,:]))**2)
    
        # I only want to take into account the distance values smaller than r0
    
        distance_matrix = np.where(distance_matrix<r0,1,0)
    
        # Variable Nj counts the number of terms per coloumn of R are taken into account
    
        Nj = np.sum(distance_matrix,axis=0)
        #print(Nj)
    
        #Define a vector R of random values in [0,2pi]
    
        R = 2*pi*np.random.rand(1,N)
    
        #Iteratively define theta at time t+1 by the given formula
    
        theta[t+1,:]=np.angle(np.dot((e**(1j*theta[t,:])),distance_matrix)+A*Nj*e**(1j*R))
        
        #print(np.dot((e**(1j*theta[t,:])),q))
    
        U[t+1,:] = s0 * np.cos(theta[t,:])
        V[t+1,:] = s0 * np.sin(theta[t,:])

        X[t+1,:] = X[t,:] + U[t+1,:]
        Y[t+1,:] = Y[t,:] + V[t+1,:]
        
        alpha[t+1]= 1/N * np.abs(np.sum(e**(1j*theta[t+1,:])))
        
    X = np.mod(X,L)
    Y = np.mod(Y,L)
    
    
    return X,Y,alpha
''' My function analyze tackles the 3 behaviours identified in the Latex report. 
    Convergence : Defined the 'confidence interval' D=(last value of Alpha -Std , last value of Alpha + Std)
    I then find the index of the first value of alpha that enters this interval. 
    Finally I conduct my analysis on this interval. 
    To eliminate any extreme cases I simulate alpha n times and then take the median of the metrics I have found.
    Inputs : 
        A : Noise level
        Nt: (As above)
        n : number of simulations required 
        
    Outputs : 
        Stdtot : Measures the Standard deviation at the stable phase
        Vartot : Measures the Variance at the stable phase
        Meantot : Measures the standard deviation at the stable phase
        alpha : Values of Alpha for our last simulation 
'''
def analyze(A,Nt,n):
   
    MeanVec=np.zeros(n)  
    VarVec=np.zeros(n)
    StdVec=np.zeros(n)
    
    for g in range (n):
    
        C,V,alpha = simulate1(32,8,0.2,1,A,Nt)
        Std1=np.std(alpha)

    
    # I'm trying to find the index of the first value that is in my interval (last value of Alpha-std,last value of Alpha+std)
    
    #H=np.array([[np.linspace(0,Nt,Nt+1)],[alpha]])
    

     
        H=np.where((alpha>(alpha[Nt]-Std1/4)) & (alpha<(alpha[Nt]+Std1/4)))[0]
        
        #This is just to deal with border case issues if the particle never enters our interval
        if len(H)==1:
            H[0]=0
        
        Std2 = np.std(alpha[H[0]:Nt+1])
        Var2 = np.var(alpha[H[0]:Nt+1])
        Mean2 = np.mean(alpha[H[0]:Nt+1])
    
        MeanVec[g]=Mean2
        VarVec[g]=Var2
        StdVec[g]=Std2

    Meantot=np.median(MeanVec)
    Stdtot=np.median(StdVec)
    Vartot=np.median(VarVec)
    return Stdtot,Vartot,Meantot,alpha



#-------------------------------------------------
    
    '''I've chosen a 'for' loop which calls the function analyze
    to produce the plots I have chosen to put forward in my report. 
    Strategy : I chose to estimate A* by finding a curve that fits our values of A vs 
    the values of alpha. I then took the derivative of this curve and found the value of A 
    at which the derivative was maximal.
    
    
    
    Variables : 
        b : number of steps for A between 0.2 and 0.8  
        Incremental of 0.6/B
        MeanAlphaArray : An array of mean alpha's produced by analyze
        StdAlphaArray : An array of standard deviations of alpha produced by analyze
        x: the different values of A 
        p : Estimated polynomial which fits our data points 
        (I used the function polyfit of numpy which uses the Least square method discovered in class)
        polyder : coefficients of the derivative of the estimated polynomial 
        Zprime : the derivative at each point of X
        Value_max: Max derivative of Zprime
        index_max: The position of Value_max in Zprime
        Astar: Estimate of A* 
        y: reduced alpha array of values satisfying our given condition
        W: Array of values (1-A/A*)       '''




#--------------------------------------------------------------
if __name__ == '__main__':
        
    '''I've chosen a 'for' loop which calls the function analyze
    to produce the plots I have chosen to put forward in my report. 
    Strategy : I chose to estimate A* by finding a curve that fits our values of A vs 
    the values of alpha. I then took the derivative of this curve and found the value of A 
    at which the derivative was maximal.
    
    
    
    Variables : 
        b : number of steps for A between 0.2 and 0.8  
        Incremental of 0.6/B
        MeanAlphaArray : An array of mean alpha's produced by analyze
        StdAlphaArray : An array of standard deviations of alpha produced by analyze
        x: the different values of A 
        p : Estimated polynomial which fits our data points 
        (I used the function polyfit of numpy which uses the Least square method discovered in class)
        polyder : coefficients of the derivative of the estimated polynomial 
        Zprime : the derivative at each point of X
        Value_max: Max derivative of Zprime
        index_max: The position of Value_max in Zprime
        Astar: Estimate of A* 
        y: reduced alpha array of values satisfying our given condition
        W: Array of values (1-A/A*)       '''


b=60
MeanAlphaArray=np.zeros(b)

StdAlphaArray=np.zeros(b)
x=np.zeros(b)
for k in range(b):
    
    X1,Y1,Z1,alpha = analyze(0.2+k/(b/0.6),200,5)
    X2,Y2,Z2,alpha2 = analyze(0.2+k/(b/0.6),200,5)
    x[k]=(0.2+k/(b/0.6))
    
    MeanAlphaArray[k]=Z1
    StdAlphaArray[k]=X1
    
    
p = np.polyfit(x,MeanAlphaArray,4)

polyder = [4,3,2,1,0]*p

Zprime = np.shape(x)

Zprime = polyder[0]*x**3+polyder[1]*x**2+polyder[2]*x+polyder[3]*np.ones(np.shape(x))

value_max = np.max(np.abs(Zprime))

index_max = np.where(np.abs(Zprime)==value_max)[0]

Astar = 0.2+(index_max)/(b/0.6)
print('My estimate for A* is: ')
print(Astar)  
    
y = x[x<Astar-0.025]
W=np.shape(y)
W=1-y/Astar
AlphaArray2=MeanAlphaArray[0:np.shape(y)[0]]

plt.figure(1)
plt.plot(x,MeanAlphaArray,'.')
plt.xlabel('A')
plt.ylabel('Stationary mean')
plt.title('Plots of of the stationary means for different values of A')
    
    
plt.figure(2)
plt.plot(x,StdAlphaArray,'.')
plt.xlabel('A')
plt.ylabel('Stationary standard deviation')
plt.title('Plots of the stationary standard deviations for different values of A')


plt.figure(4)
plt.plot(W,AlphaArray2,'.')
plt.xlabel('1-(A/A*)')
plt.ylabel('alpha')
plt.title('The dependence of alpha on 1-A/A*')
   
for k in range(7):
    
    X1,Y1,Z1,alpha = analyze(0.2+k/10,200,5)
    A=0.2+k/10
    
    plt.figure(3)
    plt.plot(np.linspace(0,np.shape(alpha)[0]-1,np.shape(alpha)[0]),alpha,label='A=')
    plt.xlabel('time (number of steps)')
    plt.ylabel('alpha')
    plt.title('Plot of simulated alpha against time for different A')
    plt.legend(['A=0.2','A=0.3','A=0.4','A=0.5','A=0.6','A=0.7','A=0.8'])
    plt.show()
    

