#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 22:25:36 2019

@author: alex
"""

import numpy as np
import matplotlib.pyplot as plt
from math import cos
from math import sin
import cmath
from math import e
from math import pi
#--------------------------------

def sim1(N=1000,L=8,s0=0.2,r0=1,Nt=100,A=0.55):


    
    phi_init = np.random.rand(N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(N))
    Xinit= r_init*np.cos(phi_init)
    Yinit= r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2

    theta_init = np.random.rand(N)*(2*np.pi) #initial directions of motion
    #---------------------

#initialize variables
    
    X = np.zeros((Nt+1,N))
    Y = np.zeros((Nt+1,N))
    theta = np.zeros((Nt+1,N))
    U = np.zeros((Nt+1,N))
    V = np.zeros((Nt+1,N))
    alpha = np.zeros((Nt+1,1))

    X[0,:] = Xinit
    Y[0,:] = Yinit
    theta[0,:] = theta_init
    distance_matrix = np.zeros((N,N))
    alpha[0] = 1/N * np.abs(np.sum(e**(1j*theta[0,:])))

    for t in range(Nt):
    

    
        # [distance_matrix]_{i,j} represents the distance between the bacteria i and j at time t.
    
        distance_matrix = np.sqrt((np.outer(np.transpose(X[t,:]),np.transpose(np.ones((N))))-np.outer(np.ones((N)),X[t,:]))**2+(np.outer(np.transpose(Y[t,:]),np.transpose(np.ones((N))))-np.outer(np.ones((N)),Y[t,:]))**2)
    
        # I only want to take into account the distance values smaller than r0
    
        distance_matrix = np.where(distance_matrix<r0,1,0)
    
        # Variable Nj counts the number of terms per coloumn of R are taken into account
    
        Nj = np.sum(distance_matrix,axis=0)
        #print(Nj)
    
        #Define a vector R of random values in [0,2pi]
    
        R = 2*pi*np.random.rand(1,N)
    
        #Iteratively define theta at time t+1 by the given formula
    
        theta[t+1,:]=np.angle(np.dot((e**(1j*theta[t,:])),distance_matrix)+A*Nj*e**(1j*R))
        
        #print(np.dot((e**(1j*theta[t,:])),q))
    
        U[t+1,:] = s0 * np.cos(theta[t,:])
        V[t+1,:] = s0 * np.sin(theta[t,:])

        X[t+1,:] = X[t,:] + U[t+1,:]
        Y[t+1,:] = Y[t,:] + V[t+1,:]
        
        alpha[t+1]= 1/N * np.abs(np.sum(e**(1j*theta[t+1,:])))
        
    X = np.mod(X,L)
    Y = np.mod(Y,L)
    
    
    return X,Y,alpha,Nt





#def analyze(A)
    Std1=np.std(alpha)
    Var1=np.var(alpha)
    Mean1=np.mean(alpha)
    
    # I'm trying to find the index of the first value that is in my interval (mean-std,mean+std)
    
    #H=np.array([[np.linspace(0,Nt,Nt+1)],[alpha]])
    H=np.where((alpha>(alpha[Nt][0]-Std1/2)) & (alpha<(alpha[Nt][0]+Std1/2)))
    print(H[0][0])
    
    Std2=np.std(alpha[H[0][0]:Nt])
    Var2=np.var(alpha[H[0][0]:Nt])
    Mean2=np.mean(alpha[H[0][0]:Nt])
    print(alpha)
    return Std2,Var2,Mean2

def sim2(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100):

    A1,A2,alpha=sim1(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100)
    
    STD,VAR,MEAN= analyse(A)
    
    return STD, VAR, MEAN
    
    
Std,Var,Mean=analyse(0.6)
print(Std,Var,Mean)
Std,Var,Mean=analyse(0.7)
print(Std,Var,Mean)'''
b=6
for j in range (b)
    X1,Y1,Z1 = analyze(0.2*j/b)
    X2,Y2,Z2 = analyze(0.2*(j+1)/b)
    
    